package com.example.missiledefence;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Daragh on 20/04/2014.
 *
 * An implementation of Projectile
 */
public class Bullet extends Projectile {

    private Paint mPaint;

    public Bullet(Bitmap bitmap, float x, float y) {
        super(bitmap, x, y);
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
    }
    
    public void reset(){
    	setLive(false);
    }

    @Override
    public void update() {
        if(!isLive()){return;}     // Only Update if Bullet is still live
        x += vector.getxVel() * getSpeed();
        y += vector.getyVel() * getSpeed();
    }

    @Override
    public void draw(Canvas canvas) {
        if(!isLive()){return;}     // Only Draw if Bullet is still live
        canvas.drawCircle(x, y, 10, mPaint);

    }
}
