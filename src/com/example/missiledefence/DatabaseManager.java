package com.example.missiledefence;
import android.content.Context;

import java.util.List;

/*
Author: Daragh
Class to manage the Database queries
Write methods here to utilise ORMLite API, through the levelDao.
TODO Catch sql exceptions
 */

public class DatabaseManager {

    static private DatabaseManager instance;

    // singleton to maintain only one db conn
    static public DatabaseManager getInstance(Context ctx) {
        if(null==instance){
            return new DatabaseManager(ctx);
        }else {
            return instance;
        }
    }

    private DatabaseHelper helper;
    private DatabaseManager(Context ctx) {
        helper = new DatabaseHelper(ctx);
    }

    private DatabaseHelper getHelper() {
        return helper;
    }

    public List<Level> getAllLevels() {
        return getHelper().getLevelDao().queryForAll();
    }

    // if we use the unique level id to determine levels,
    // we can retrieve levels like this
    public Level getLevelByNumber(int levelNumber) {
        return getHelper().getLevelDao().queryForId(levelNumber);
    }
}