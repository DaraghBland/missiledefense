package com.example.missiledefence;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class Dude {

    private int FRAMEWIDTH = 128; // smurfsprite is 512x512
    private int FRAMEHEIGHT = 128;
    private int FRAMESPEED = 10;
    private int ENDFRAME = 15;

	private Bitmap bitmap;		// Image of the dude
	private Paint mPaint;       // Used as crosshair on dude
	private float x, y;			// position on screen
	private float mWidth, mHeight;
	private boolean touched;	// is dude being held
	private Vector vector;		// dude's speed and direction (vector)
	private boolean isLive;
	private int refireRate;
    private Spritesheet spritesheet;
    
    private Zone myZone;
	
	public Dude(Bitmap bitmap, int x, int y){
		this.bitmap = bitmap;
		this.x = x;
		this.y = y;
		this.vector = new Vector();
		this.touched = false;
		this.isLive = false;
		this.refireRate = 50;
		mPaint = new Paint();
		mPaint.setColor(Color.RED);
        spritesheet = new Spritesheet(bitmap, FRAMEWIDTH, FRAMEHEIGHT, FRAMESPEED, ENDFRAME);
	}
	
	public Bitmap getBitmap(){
		return this.bitmap;
	}
	
	public void setBitmap(Bitmap bitmap){
		this.bitmap = bitmap;
	}
	
	public float getX(){
		return this.x;
	}
	
	public void setX(float xPos){
		this.x = xPos;
	}
	
	public float getY(){
		return this.y;
	}
	
	public void setY(float yPos){
		this.y = yPos;
	}
	
	public float getWidth() {
		return mWidth;
	}

	public void setWidth(float width) {
		this.mWidth = width;
	}

	public float getHeight() {
		return mHeight;
	}

	public void setHeight(float height) {
		this.mHeight = height;
	}

	public boolean isTouched(){
		return this.touched;
	}
	
	public void setTouched(boolean touched){
		this.touched = touched;
	}
	
	public boolean isLive(){
		return isLive;
	}
	
	public void setLive(boolean live){
		this.isLive = live;
	}
	
	public Vector getVector(){
		return this.vector;
	}
	
	public void setSpeed(Vector vector){
		this.vector = vector;
	}
	
	public void setZone(Zone zone){
		this.myZone = zone;
	}
	
	public Zone getZone(){
		return myZone;
	}

	public void fire(){

        Bullet b = new Bullet(null, getX(), getY());
        b.setVector(new Vector(0,1));
        b.setLive(true);
        ProjectileManager.getInstance().getDudeProjectiles().add(b);
        this.refireRate = 50;

	}

	public void draw(Canvas canvas){
		if(!isLive)
			return;
        Bitmap c = Bitmap.createBitmap(spritesheet.getSheet(), (int)(spritesheet.getImgCoords().getX()), (int)(spritesheet.getImgCoords().getY()), FRAMEWIDTH, FRAMEHEIGHT);
        setWidth(c.getWidth());
        setHeight(c.getHeight());
		canvas.drawBitmap(c, x - (c.getWidth()/2), y - (c.getHeight()/2), null);
		mPaint.setAlpha(255);
		canvas.drawCircle(x, y, 10, mPaint);
		canvas.drawText(refireRate+"", x+20, y, mPaint);
		if(myZone != null){
			mPaint.setAlpha(128);
			if(myZone.getOrientation() == Zone.VERTICAL)
				canvas.drawLine(myZone.getCenterX(), myZone.getTop(), myZone.getCenterX(), myZone.getBottom(), mPaint);
			else
				canvas.drawLine(myZone.getLeft(), myZone.getCenterY(), myZone.getRight(), myZone.getCenterY(), mPaint);
			
		}
	}
	
	// Update function. Adjust dude's position every "tick"
	public void update(){

		if(!isLive)
			return;
		if(!touched){
			if(myZone != null){
				int outside = myZone.isOutsideRange((int) x, (int) y);
				if(outside != Zone.INSIDE){
					if(outside == Zone.OUT_LEFT || outside == Zone.OUT_RIGHT)
						vector.toggleX();
					if(outside == Zone.OUT_TOP || outside == Zone.OUT_BOTTOM)
						vector.toggleY();
					if(outside == Zone.OUT_OFF_PATH){
						x = myZone.getCenterX();
						y = myZone.getCenterY();
					}
				}
			}
			
			x += vector.getxVel();
            y += vector.getyVel();
            	
            if(refireRate == 0)
            	fire();
            else
            	refireRate--;   

        spritesheet.update();
		}
	}
	
	// Handle touch event
	public void handleActionDown(float eventX, float eventY){
		if(!isLive)
			return;
		if(eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth() / 2))){
			if(eventY >= (y - bitmap.getHeight() / 2) && ( eventY <= (y + bitmap.getHeight() / 2))){
				setTouched(true);
			}else{
				setTouched(false);
			}
		}else{
			setTouched(false);
		}
	}

}
