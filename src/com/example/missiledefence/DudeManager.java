package com.example.missiledefence;

import java.util.ArrayList;
import java.util.ListIterator;

import android.graphics.Bitmap;
/**
 * 
 * @author Derek 16-11-2014
 *
 */

public class DudeManager {
	
	private ArrayList<Dude> dudes;
	private int dudeSpawnTime = 0, maxDudeSpawnTime = 200;
	private int maxDudesOnScreen = 10;
	private Bitmap enemyBitmap;
	private DudeManagerListener dmListen;
	
	public interface DudeManagerListener{
		void onDudeDeath();
	}
	
	public void setDudeManagerListener(DudeManagerListener dmListen){
		this.dmListen = dmListen;
	}
	
	public DudeManager(){
		
	}
	
	public DudeManager(ArrayList<Dude> suppliedDudes){
		this.dudes = suppliedDudes;
	}
	
	public void setDudes(ArrayList<Dude> newDudes) {
		this.dudes = newDudes;
	}
	
	public ArrayList<Dude> getAllTheDudes() {
		return this.dudes;
	}
	
	public Bitmap getEnemyBitmap() {
		return enemyBitmap;
	}

	public void setEnemyBitmap(Bitmap enemyBitmap) {
		this.enemyBitmap = enemyBitmap;
	}
	
	public void removeDeadDudes() {
		final ListIterator<Dude> iter = dudes.listIterator();
		while(iter.hasNext()){
			Dude dude = iter.next();
			if(!dude.isLive()){
				iter.remove();
				if(dmListen != null)
					dmListen.onDudeDeath();
			}
		}
	}

	public void spawnNewDude() {
		// Not time yet
		if(dudeSpawnTime < maxDudeSpawnTime){
			dudeSpawnTime++;
			return;
		}
		
		// Time for a new dude
		dudeSpawnTime = 0;
		
		// Max dudes on screen already, exit
		if(dudes.size() >= maxDudesOnScreen)
			return;
		
		// Otherwise new dude
		Dude dude = new Dude(enemyBitmap, 50, 50);
		dude.setX((float) ((Math.random() * 500) + 100));
		dude.setY((float) ((Math.random() * 500) + 100));
		if(dudes.size() % 2 == 0){
			dude.setSpeed(new Vector((float) (Math.random() * 10) + 1, 0));
			Zone zone = new Zone(Zone.HORIZONTAL, (int) dude.getX(), (int) dude.getY());
			dude.setZone(zone);
		}else{
			dude.setSpeed(new Vector(0, (float) (Math.random() * 10) + 1));
			Zone zone = new Zone(Zone.VERTICAL, (int) dude.getX(), (int) dude.getY());
			dude.setZone(zone);
		}
		dude.setLive(true);
		dudes.add(dude);
		
		
	}

}
