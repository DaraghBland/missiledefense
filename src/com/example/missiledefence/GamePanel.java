package com.example.missiledefence;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = GamePanel.class.getSimpleName();
    private Level levelOne = DatabaseManager.getInstance(this.getContext()).getLevelByNumber(1);
    int introCounter = 300;
    
    public static final int INTRO_STATE = 0;
    public static final int GAME_STATE = 1;
    public static final int FINISH_STATE = 2;
    private int currentState = INTRO_STATE;

    enum gameState {INTRO_STATE, GAME_STATE, FINISH_STATE};

    public interface GamePanelListener{
    	void onGameStart();
    	void onGameOver();
    	void onGameOverSuccess();
    }

    private GameThread gameThread;
    private Player player;
    private ProjectileManager projectileManager = ProjectileManager.getInstance();
    private DudeManager dudeManager;
    private HUD hud;
    private SoundPoolManager spm;
    private int soundID;
    private boolean loaded;
    
    private GamePanelListener gpListener = null;

    public GamePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpGamePanel();
    }

    public GamePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setUpGamePanel();
    }

    public GamePanel(Context context) {
        super(context);
        setUpGamePanel();

    }
    
    public void setGamePanelListener(GamePanelListener gpl){
    	this.gpListener = gpl;
    }

    public void setUpGamePanel() {

        // Add holder. Can intercept events
        getHolder().addCallback(this);

        // Set up SoundPool
        spm = new SoundPoolManager();
        spm.getSoundPool().setOnLoadCompleteListener(new
        SoundPool.OnLoadCompleteListener() {
        @Override
        public void onLoadComplete (SoundPool soundPool,int sampleId, int status){
               loaded = true;
           }
        });
        soundID = spm.getSoundPool().load(getContext(), R.raw.gunshot, 1);

        // Make a call to the database to get Level

        // Create moving guy
        ArrayList<Dude> dudes = new ArrayList<Dude>();
        
        //int startingDudes = 4;
        int startingDudes = levelOne.getStartingDudes();
        Bitmap enemyBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.smurfsprite);
        for (int i = 0; i < startingDudes; i++) {
            Dude temp = new Dude(enemyBitmap, 50, 50);
            temp.setX((float) ((Math.random() * 5) + 50));
            temp.setY((float) ((Math.random() * 5) + 50));
            temp.setSpeed(new Vector((float) (Math.random() * 5) + 1, (float) (Math.random() * 5) + 1));
            dudes.add(temp);
        }
        
        dudeManager = new DudeManager(dudes);
        dudeManager.setEnemyBitmap(enemyBitmap);
        dudeManager.setDudeManagerListener(new DudeManager.DudeManagerListener() {
			
			@Override
			public void onDudeDeath() {
				hud.enemyDeaths++;
				
			}
		});
        //dudeManager.setDudes(dudes);

        player = new Player(null, 0, 0, 10, 10);
        player.setPlayerListener(new Player.PlayerListener() {
			
			@Override
			public void onPlayerDeath() {
				hud.deaths++;
                spm.getSoundPool().play(soundID, 0.8f, 0.8f, 1, 0, 1f);
			}
		});
        
        hud = new HUD();

        // Create thread
        gameThread = new GameThread(getHolder(), this);

        // Make it focusable, so we can get events
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "Surface changing...");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // Surface is created. Start Loop
        player.setPosX(getWidth() / 2);
        player.setPosY(getHeight() * 0.9f);
        player.setWidth(getWidth());
        player.setHeight(getHeight() * 0.2f);

        for (Dude dude : dudeManager.getAllTheDudes()) dude.setLive(true);

        gameThread = new GameThread(getHolder(), this);
        gameThread.setRunning(true);
        gameThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "Destroying Game Panel...");
        // Notify thread to shut down. Wait for it to do so
        boolean retry = true;
        gameThread.setRunning(false);
        while (retry) {
            try {
                gameThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "Game Thread shut down");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            player.handleActionDown(event.getX(), event.getY());
        }

        return true;
    }

    public void render(Canvas canvas){
    	if(canvas == null)
    		return;
    	
    	canvas.drawColor(Color.BLACK); // TODO: Replace with actual background
    	player.draw(canvas);
    	hud.draw(canvas);
    	
    	switch(currentState){
    	case INTRO_STATE:
    		introRender(canvas);
    		break;
    	case GAME_STATE:
    		gameRender(canvas);
    		break;
    	}
    }
    
    public void introRender(Canvas canvas){
    	// TODO: draw intro stuff
    }
    
    public void gameRender(Canvas canvas){
    	for(Dude dude : dudeManager.getAllTheDudes()){
    		dude.draw(canvas);
    	}
    	
    	projectileManager.playerDraw(canvas);
    	projectileManager.dudeDraw(canvas);
    }

    /**
     * This is where all the game updates happen. We loop through any objects in game and call
     * their update functions.
     */
    public void update() {
        switch(currentState){
            case INTRO_STATE:
                introUpdate();
                break;
            case GAME_STATE:
                gameUpdate();
                break;
            case FINISH_STATE:
                outroUpdate();
                break;
        }
    }

    // 3 second delay
    private void introUpdate() {
        if(introCounter != 0){
            introCounter--;
        }else{
            currentState = GAME_STATE;
        }
    }

    private void gameUpdate(){

        // check for state change
        if(hud.enemyDeaths >= levelOne.getEnemyDeathsRequired()){
            if(gpListener != null)
                gpListener.onGameOverSuccess();
                currentState = FINISH_STATE;
        }

        // Collision Check
        Point bullet = new Point(0, 50);

        // Boundary check for player's projectile
        if (!projectileManager.getPlayerProjectiles().isEmpty()) {
            for (Projectile p : projectileManager.getPlayerProjectiles()) {
                bullet = new Point(p.getX(), p.getY());
                if (bullet.getX() > getWidth() || bullet.getX() < 0 || bullet.getY() > getHeight() || bullet.getY() < 0)
                    p.reset();
            }
        }

        dudeManager.removeDeadDudes();
        // Boundary check on Dudes
        for (Dude droid : dudeManager.getAllTheDudes()) {

            if(droid.isLive()) {

                // Right Wall
                if (droid.getX() + droid.getWidth() / 2 >= getWidth() && droid.getVector().getxVel() > 0) {
                    droid.getVector().toggleX();
                }

                // Left Wall
                if (droid.getX() - droid.getWidth() / 2 <= 0 && droid.getVector().getxVel() < 0) {
                    droid.getVector().toggleX();
                }

                // Bottom Wall
                if (droid.getY() + droid.getHeight() / 2 >= getHeight() * 0.8f && droid.getVector().getyVel() > 0) {
                    droid.getVector().toggleY();
                }

                // Top Wall
                if (droid.getY() - droid.getHeight() / 2 <= 0 && droid.getVector().getyVel() < 0) {
                    droid.getVector().toggleY();
                }

                // Check for being hit by Enemy Projectile
                for (Projectile p : projectileManager.getDudeProjectiles()) {

                    if (p.isLive()) {
                        if (p.getY() > (getHeight() - player.getHeight())) {
                            player.takeDamage(5);
                            p.reset();
                        }

                        if (p.getX() > getWidth() || p.getX() < 0 || p.getY() < 0)
                            p.reset();
                    }

                }

                // Check for hitting Enemy with Player Projectile
                for (Projectile pr : projectileManager.getPlayerProjectiles()) {
                    // Hit by bullet
                    // Using circular collision box
                    if (pr.isLive()) {
                        float d = (float) Math.sqrt(Math.pow(bullet.getX() - droid.getX(), 2) + Math.pow(bullet.getY() - droid.getY(), 2));
                        if (d <= 10 + (droid.getWidth() / 2)) {
                            droid.setLive(false);
                            pr.reset();
                        }
                    }
                }
            }

            /**
             * Update the Dudes projectile status
             * This involves clearing the list of all "dead" projectiles
             * and calling update on the remaining ones
             */
            projectileManager.dudeUpdate();

            // Update objects
            droid.update();
        }
        player.update();

        /**
         * Update the Player's projectile status
         * This involves clearing the list of all "dead" projectiles
         * and calling update on the remaining ones
         */
        projectileManager.playerUpdate();

        // Add new dude if MaxNoOfDudes has not been reached
        dudeManager.spawnNewDude();

    }

    // 3 second delay
    private void outroUpdate() {
        gpListener.onGameOver();
    }


}
