package com.example.missiledefence;

import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;

public class GameThread extends Thread{
	
	private static final String TAG = GameThread.class.getSimpleName();
	
	// desired FPS
	private static final int	MAX_FPS = 50;
	// maximum number of frames to be skipped
	private static final int	MAX_FRAME_SKIPS = 5;
	// frame period
	private static final int	FRAME_PERIOD = 1000 / MAX_FPS;
	
	// SurfaceHolder that can access the physical surface
	private final SurfaceHolder surfaceHolder;
	// The actual view that handles input and draws the surface
	private GamePanel gamePanel;
	
	// Flag to hold game state
	private boolean run;
	public void setRunning(boolean running){
		this.run = running;
	}
	
	public GameThread(SurfaceHolder surfaceHolder, GamePanel gamePanel){
		super();
		this.gamePanel = gamePanel;
		this.surfaceHolder = surfaceHolder;
	}
	
	@Override
	public void run(){
		Canvas canvas;
		Log.d(TAG, "Starting Game Loop...");
		
		long beginTime;		// Time when the cycle began
		long timeDiff;		// Time it took for cycle to execute
		long sleepTime;		// ms to sleep (if we're behind, no sleep)
		int framesSkipped;	// number of frames skipped
		
		sleepTime = 0;
		while(run){
			canvas = null;
			// Lock the canvas so we aren't messing up other draw functions
			try{
				canvas = this.surfaceHolder.lockCanvas();
				synchronized(surfaceHolder){
					beginTime = System.currentTimeMillis();
					framesSkipped = 0;	// Reset frames skipped
					// Update
					gamePanel.update();
					// Render and Draw
					gamePanel.render(canvas);
                    gamePanel.postInvalidate();
                    // Calculate how long did the cycle take
                    timeDiff = System.currentTimeMillis() - beginTime;
                    // Calculate sleep time
                    sleepTime = (int)(FRAME_PERIOD - timeDiff);
                    
                    if(sleepTime > 0){
                    	// if sleep time > 0, we can rest
                    	try{
                    		// send thread to sleep for a bit
                    		Thread.sleep(sleepTime);
                    	}catch(InterruptedException e){
                    		e.printStackTrace();
                    		Log.e(TAG, e.getMessage());
                    	}
                    }
                    
                    while(sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS){
                    	// we're behind
                    	this.gamePanel.update();	// update, but no render
                    	sleepTime += FRAME_PERIOD;	// add frame period to check if in next frame
                    	framesSkipped++;
                    }
                    
				}
			}finally{
				// Release the canvas
				if(canvas != null){
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

}
