package com.example.missiledefence;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Gun class
 * @author Derek
 *
 */
public class Gun {
    private static final String TAG = Gun.class.getSimpleName();
    private Bitmap image;
    private Paint mPaint;
	private float posX, posY;
	private float width, height;
	private float turnSpeed;
	private float turnAngle, targetAngle;
	private Point target;
    private Boolean hasTarget, onTarget;
    private int refireRate;

    private Vector targetVector;

	public Gun(Bitmap image, float posX, float posY, float turnSpeed){
		this.image = image;
		this.posX = posX;
		this.posY = posY;
		this.width = 10;
		this.height = 50;
		this.turnSpeed = turnSpeed;
		this.turnAngle = 0;
		this.targetAngle = 0;
        this.hasTarget = false;
        this.onTarget = false;
		this.target = new Point(posX, posY-100);
        this.refireRate = 0;
		
		mPaint = new Paint();
		mPaint.setColor(Color.BLUE);
	}

	
	/**
	 * Method to set the Gun image
	 * @param image Bitmap to be set
	 */
	public void setImage(Bitmap image){	this.image = image;	}
	/**
	 * Method to retrieve the Gun image
	 * @return Bitmap image of the Gun
	 */
	public Bitmap getImage(){	return this.image;	}
	
	/**
	 * Convenience method to set both X,Y coords
	 * @param posX new X position
	 * @param posY new Y position
	 */
	public void setPosition(float posX, float posY){
		setPosX(posX);
		setPosY(posY);
	}
	/**
	 * Method to set the X coord of the Gun
	 * @param posX new X position
	 */
	public void setPosX(float posX){	this.posX = posX; }
	/**
	 * Method to set the X coord of the Gun
	 * @param posY new Y position
	 */
	public void setPosY(float posY){	this.posY = posY; }
	/**
	 * Method to retrieve X coord of Gun
	 * @return float value of X coord
	 */
	public float getPosX(){	return this.posX;	}
	/**
	 * Method to retrieve X coord of Gun
	 * @return float value of Y coord
	 */
	public float getPosY(){	return this.posY;	}
	
	public float getWidth() {	return width;	}
	public void setWidth(float width) {	this.width = width;	}

	public float getHeight() {	return height;	}
	public void setHeight(float height) {	this.height = height;	}

	/**
	 * Method to set the turning speed of the Gun
	 * @param turnSpeed float value for the turning speed
	 */
	public void setTurnSpeed(float turnSpeed){	this.turnSpeed = turnSpeed;	}
	/**
	 * Method to retrieve the turning speed of the Gun
	 * @return float value of the turning speed
	 */
	public float getTurnSpeed(){	return this.turnSpeed;	}
	/**
	 * Method to set the angle the Gun is turned to
	 * @param turnAngle float value of turning angle. Wrapped 0 - 360
	 */
	public void setTurnAngle(float turnAngle){	this.turnAngle = turnAngle;	}
	/**
	 * Method to retrieve the Gun's turning angle
	 * @return	float value of turning angle. Between 0 - 360
	 */
	public float getTurnAngle(){	return this.turnAngle;	}
	
	public void setTarget(Point target){
		this.target = target;
		
		// Calculate targetAngle
		Vector centreLine = new Vector(0, -1);
		targetVector = new Vector(target.getX() - posX, target.getY() - (posY+(height/2)));
		targetVector = Vector.normalise(targetVector);
		double dotProd = targetVector.dotProduct(centreLine);
		double angleInRads = Math.acos(dotProd);
		this.targetAngle = (float)(angleInRads * (180/Math.PI));
		if(target.getX() < posX)
			targetAngle *= -1;
	}
	public Point getTarget(){ return this.target; }

    public Vector getTargetVector() { return targetVector; }

    public void setTargetVector(Vector targetVector) { this.targetVector = targetVector; }
	
	public void setTargetAngle(float targetAngle){	this.targetAngle = targetAngle;	}
	public float getTargetAngle(){	return this.targetAngle;	}

    public Boolean getHasTarget() {
        return hasTarget;
    }

    public void setHasTarget(Boolean hasTarget) {
        this.hasTarget = hasTarget;
    }
	
	public void update(){
        float diffAngle = targetAngle - turnAngle;
        onTarget = false;
		if(diffAngle < turnSpeed && diffAngle > -turnSpeed){
			turnAngle = targetAngle;
			onTarget = true;
		}else{
			if(diffAngle > 0)
				turnAngle += turnSpeed;
			else
				turnAngle -= turnSpeed;
		}

        // Fire the projectile once the Gun has it's target
        if(hasTarget && onTarget && refireRate <= 0)
            fireProjectile();

        // Cooldown for the player's projectile
        if(refireRate > 0)
            refireRate--;

	}

    /**
     * Create a new Bullet and add it to the Player's list of projectiles.
     * Set attributes of the bullet such as position, isLive, target and fire rate.
     */
	public void fireProjectile(){
        Bullet bullet = new Bullet(null, posX, posY + 50);
		bullet.setLive(true);
        bullet.setVector(targetVector);
		hasTarget = false;
        this.refireRate = 50;
        ProjectileManager.getInstance().getPlayerProjectiles().add(bullet);

    }
	
	public void draw(Canvas canvas){
		canvas.save();
		canvas.rotate(turnAngle, posX, posY+50);
		canvas.drawRect(posX-10, posY-50, posX+10, posY+50, mPaint);

		// TargetLine
		canvas.drawLine(posX, posY+50, posX, posY-1000, mPaint);
		canvas.restore();
		
		if(turnAngle == targetAngle)
			canvas.drawText("On Target", posX-100, posY, mPaint);
		else
			canvas.drawText(""+turnAngle, posX-100, posY, mPaint);
	}

}
