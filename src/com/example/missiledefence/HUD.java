package com.example.missiledefence;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Class to display HUD data, replace with android widgets
 * @author Derek
 *
 */
public class HUD {
	
	private final static int TEXT_SIZE = 50;
	
	private Paint mPaint;
	public int enemyDeaths = 0;
	public int deaths = 0;
	
	public HUD(){
		mPaint = new Paint();
		mPaint.setColor(Color.WHITE);
		mPaint.setTextSize(TEXT_SIZE);
	}
	
	private int getKDRatio(){
		if(deaths <= 0)
			return enemyDeaths;
		
		return enemyDeaths/deaths;
	}
	
	public void draw(Canvas c){
		c.save();
		c.drawText("Deaths:" + deaths, 0, TEXT_SIZE, mPaint);
		c.drawText("Enemy Deaths: "+ enemyDeaths, 0, 2*TEXT_SIZE, mPaint);
		c.drawText("K/D: "+getKDRatio(), 0, 3*TEXT_SIZE, mPaint);
		c.restore();
	}

}
