package com.example.missiledefence;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Daragh on 01/03/2015.
 * Class to represent a Level
 */
    @DatabaseTable(tableName = "Level")
    public class Level {

        @DatabaseField(generatedId = true)
        int id;
        @DatabaseField
        private int startingDudes;
        @DatabaseField
        private int enemyDeathsRequired;

        public Level() {
            // ORMLite needs a no-arg constructor
        }

        public Level(int startingDudes, int enemyDeathsRequired) {
            this.startingDudes = startingDudes;
            this.enemyDeathsRequired = enemyDeathsRequired;
        }

        public int getStartingDudes() {
            return startingDudes;
        }
        public void setStartingDudes(int startingDudes) {
            this.startingDudes = startingDudes;
        }

        public int getEnemyDeathsRequired() {
            return enemyDeathsRequired;
        }

        public void setEnemyDeathsRequired(int enemyDeathsRequired) {
            this.enemyDeathsRequired = enemyDeathsRequired;
        }

    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", startingDudes=" + startingDudes +
                ", enemyDeathsRequired=" + enemyDeathsRequired +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Level level = (Level) o;

        if (enemyDeathsRequired != level.enemyDeathsRequired) return false;
        if (id != level.id) return false;
        if (startingDudes != level.startingDudes) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + startingDudes;
        result = 31 * result + enemyDeathsRequired;
        return result;
    }
}
