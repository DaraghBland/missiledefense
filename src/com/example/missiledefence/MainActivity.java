package com.example.missiledefence;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends Activity implements GamePanel.GamePanelListener{

	private static final String TAG = MainActivity.class.getSimpleName();

	private Button beginButton;
	private GamePanel gamePanel;

	private Handler handler = new Handler();

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// No Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Full Screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// Set Game Panel
		setContentView(R.layout.activity_main);

		findViews();
		setListeners();

		Log.d(TAG, "View Added");
    }

	private void findViews(){
		beginButton = (Button) findViewById(R.id.beginButton);

		gamePanel = (GamePanel) findViewById(R.id.gamePanel);
	}

	private void setListeners(){
		beginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				beginButton.setVisibility(View.INVISIBLE);

			}
		});

		gamePanel.setGamePanelListener(this);
	}

	@Override
	public void onDestroy(){
		Log.d(TAG, "Destroying...");
		super.onDestroy();
	}

	@Override
	public void onStop(){
		Log.d(TAG, "Stopping...");
		super.onStop();
	}

    @Override
	public void onResume(){
		Log.d(TAG, "Resuming...");
		super.onResume();
	}

    @Override
	public void onPause(){
		Log.d(TAG, "Pausing...");
		super.onPause();
	}

	@Override
	public void onGameStart() {
        beginButton.setText("Beginning!");
        beginButton.setVisibility(View.VISIBLE);
	}

	@Override
	public void onGameOver() {

		handler.post(new Runnable() {

			@Override
			public void run() {
				beginButton.setText("GAME OVER!");
				beginButton.setVisibility(View.VISIBLE);

				beginButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();

					}
				});
			}

		});
	}

	@Override
	public void onGameOverSuccess() {
		handler.post(new Runnable(){

			@Override
			public void run() {
				beginButton.setText("WELL DONE!");
				beginButton.setVisibility(View.VISIBLE);
				
				beginButton.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						finish();
						
					}
				});
			}

		});



	}

}
