package com.example.missiledefence;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainMenuActivity extends Activity {

	private Button start, truFacks;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_menu);

		findViews();
		setViewListeners();

    }

	private void findViews(){
		start = (Button) findViewById(R.id.startButton);
		truFacks = (Button) findViewById(R.id.trufacks);

	}
	
	private void setViewListeners(){
		start.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startNextActivity();
				
			}
		});
		
		truFacks.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.truFack1), Toast.LENGTH_SHORT).show();
				
			}
		});
	}
	
	private void startNextActivity(){
		startActivity(new Intent(this, MainActivity.class));
	}

}
