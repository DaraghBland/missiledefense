package com.example.missiledefence;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Player class
 * @author Derek
 *
 */
public class Player {
	private Bitmap image;
	private Paint mPaint;
	private float posX, posY;
	private float width, height;
	private Gun mainGun;
	private int health;
	private PlayerListener pListen;
	
	public interface PlayerListener{
		void onPlayerDeath();
	}
	
	public Player(Bitmap image, float posX, float posY, float width, float height){
		this.image = image;
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
		this.health = 255;
		this.mainGun = new Gun(null, posX, posY, 2);
		
		mPaint = new Paint();
		mPaint.setColor(Color.GREEN);
	}
	
	public void setPlayerListener(PlayerListener pListen){
		this.pListen = pListen;
	}
	
	public Bitmap getImage(){	return this.image;	}
	public void setImage(Bitmap image){	this.image = image;	}
	
	public float getPosX(){	return this.posX;	}
	public void setPosX(float posX){
		this.posX = posX;
		mainGun.setPosX(posX);
	}
	
	public float getPosY(){	return this.posY;	}
	public void setPosY(float posY){
		this.posY = posY;
		mainGun.setPosY(posY);
	}
	
	public float getWidth(){	return this.width;	}
	public void setWidth(float width){	this.width = width;	}
	
	public float getHeight(){	return this.height;	}
	public void setHeight(float height){	this.height = height;	}
	
	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
	
	public void takeDamage(int damage){
		health -= damage;
		
		// IMMORTALITY!!! mwhahahahaha
		if(health < 0){
			health = 255;
			if(pListen != null)
				pListen.onPlayerDeath();
		}
		
	}

	public Gun getGun(){	return this.mainGun;	}
	public void setGun(Gun newGun){	this.mainGun = newGun;	}
	
	// Handle touch event
	public void handleActionDown(float eventX, float eventY){
		mainGun.setTarget(new Point(eventX, eventY));
		mainGun.setHasTarget(true);
			
	}

    /**
     *
     * @param x Position the user tapped on screen
     * @param y is used to direct the projectile.
     *          Also sets the Projectile to live
     *          to indicate that it has been fired
     *          and is active.
     */
//    public void fireProjectile(float x, float y){
//
//        // Fire Bullet when user taps screen
//        mainGun.setHasTarget(true);
//        if(!mainGun.getBullets()[0].isLive()) {
//            mainGun.getBullets()[0].setLive(true);
//            mainGun.getBullets()[0].setVector(mainGun.getTargetVector());
//        }
//
//    }
	
	public void update(){
		mainGun.update();
	}
	
	public void draw(Canvas canvas){
		canvas.save();
		mPaint.setAlpha(health);
		canvas.drawRect(posX-(width/2), posY-(height/2), posX+(width/2), posY+(height/2), mPaint);
		canvas.restore();
		
		mainGun.draw(canvas);
	}

}
