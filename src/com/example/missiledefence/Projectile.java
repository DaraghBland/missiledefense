package com.example.missiledefence;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Daragh on 14/04/2014.
 *
 * Abstract class to represent instances of Projectile
 */
public abstract class Projectile {

    public Bitmap bitmap;		// Image of the projectile
    public float x, y;			// position on screen
    private boolean isLive;	// is projectile live
    private float speed;	// pixel movement per tick
    public Vector vector;		// projectile's speed and direction (vector)


    public Projectile(Bitmap bitmap, float x, float y){
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        this.speed = 20;
        this.vector = new Vector();
        this.isLive = false;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        this.isLive = live;
    }

    public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public Vector getVector() {
        return vector;
    }

    public void setVector(Vector vector) {
        this.vector = vector;
    }
    
    public abstract void reset();

    public abstract void update();

    public abstract void draw(Canvas canvas);
}
