package com.example.missiledefence;

import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Daragh on 18/05/2014.
 */
public class ProjectileManager {

    private static final String TAG = GamePanel.class.getSimpleName();

    private static List<Projectile> playerProjectiles = new ArrayList<Projectile>();
    private static List<Projectile> dudeProjectiles = new ArrayList<Projectile>();

    private static ProjectileManager instance = null;

    protected ProjectileManager() {
        // Just so it can't be instantiated
    }

    public void playerUpdate() throws ConcurrentModificationException {

        // Remove "dead" playerProjectiles from the list
        for (final ListIterator<Projectile> iter = playerProjectiles.listIterator(); iter.hasNext(); ) {
            Projectile element = iter.next();
            if (!element.isLive()) {
                iter.remove();
            }
        }

        // Call update on every projectile
        for (Projectile p : playerProjectiles)
            p.update();
    }

    public void dudeUpdate() throws ConcurrentModificationException {

        // Remove "dead" dudeProjectiles from the list
        for (final ListIterator<Projectile> iter = dudeProjectiles.listIterator(); iter.hasNext(); ) {
            Projectile element = iter.next();
            if (!element.isLive()) {
                iter.remove();
            }
        }

        // Call update on every projectile
        for (Projectile p : dudeProjectiles)
            p.update();
    }

    public void playerDraw(Canvas canvas) {
        for (Projectile p : playerProjectiles)
            p.draw(canvas);
    }

    public void dudeDraw(Canvas canvas) {
        for (Projectile p : dudeProjectiles)
            p.draw(canvas);
    }


    public static ProjectileManager getInstance() {

        if (instance == null) {
            instance = new ProjectileManager();
        }
        return instance;
    }

    public List<Projectile> getPlayerProjectiles() {
        return playerProjectiles;
    }

    public List<Projectile> getDudeProjectiles() {
        return dudeProjectiles;
    }

}


