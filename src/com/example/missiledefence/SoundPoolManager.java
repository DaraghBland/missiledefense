package com.example.missiledefence;

import android.app.Activity;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundPoolManager extends Activity {
    private SoundPool soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
    boolean loaded = false;
    /*float actVolume, maxVolume, volume;
    AudioManager audioManager;*/

    public SoundPool getSoundPool(){
        return soundPool;
    }

    public boolean getLoaded() {
        return loaded;
    }

/*  Error: System services not available before onCreate()
    public float getVolume(){
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        actVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actVolume / maxVolume;
        return volume;
    }*/


}
