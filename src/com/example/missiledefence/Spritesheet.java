package com.example.missiledefence;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Daragh on 23/11/2014.
 */
public class Spritesheet {

    private static final String TAG = GamePanel.class.getSimpleName();

    Bitmap sheet;
    int currentFrame;  // the current frame to draw
    int frameWidth;
    int frameHeight;
    int frameSpeed;
    int endFrame;
    int counter = 0;       // keep track of frame rate
    int framesPerRow;
    int framesPerCol;
    Point imgCoords[];

    public Point getImgCoords() {
        return imgCoords[counter];
    }

    public void setImgCoords(Point[] imgCoords) {
        this.imgCoords = imgCoords;
    }

    public int getFrameSpeed() {
        return frameSpeed;
    }

    public void setFrameSpeed(int frameSpeed) {
        this.frameSpeed = frameSpeed;
    }

    public int getEndFrame() {
        return endFrame;
    }

    public void setEndFrame(int endFrame) {
        this.endFrame = endFrame;
    }

    public int getCurrentFrame() {
        return currentFrame;
    }

    public void setCurrentFrame(int currentFrame) {
        this.currentFrame = currentFrame;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Bitmap getSheet() {
        return sheet;
    }

    public void setSheet(Bitmap sheet) {
        this.sheet = sheet;
    }

    public int getFrameWidth() {
        return frameWidth;
    }

    public void setFrameWidth(int frameWidth) {
        this.frameWidth = frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    public void setFrameHeight(int frameHeight) {
        this.frameHeight = frameHeight;
    }

    public int getFramesPerRow() {
        return framesPerRow;
    }

    public void setFramesPerRow(int framesPerRow) {
        this.framesPerRow = framesPerRow;
    }

    public int getFramesPerCol() {
        return framesPerCol;
    }

    public void setFramesPerCol(int framesPerCol) {
        this.framesPerCol = framesPerCol;
    }

    public Spritesheet(Bitmap sheet, int frameWidth, int frameHeight, int frameSpeed, int endFrame) {
        this.sheet = sheet;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.framesPerRow = sheet.getWidth()/frameWidth;
        this.framesPerCol = sheet.getHeight()/frameHeight;
        this.frameSpeed = frameSpeed;
        this.endFrame = endFrame;
        this.imgCoords = new Point[framesPerRow * framesPerCol];
        populateCoordinates();
    }

    public void update(){

        // update to the next frame if it is time
        if (counter == (frameSpeed - 1))
            currentFrame = (currentFrame + 1) % endFrame;

        // update the counter
        counter = (counter + 1) % frameSpeed;

    }

    /*
    *  Populate the imgCoords array with the positions of the individual Sprite frames within the Sprite sheet
    *  Assumes that the frameWidth is the same for every frame
    *
    *  May need to make a 2D array to handle whole spritesheets (i.e. frames per column as well as rows)
    *
    * */
    public void populateCoordinates(){
        int i = 0;
        for(int y = 0; y < this.sheet.getHeight(); y += frameHeight){
            for(int x = 0; x < this.sheet.getWidth(); x += frameWidth){
                imgCoords[i] = new Point(x, y);
                i++;
                Log.i(TAG, "x,y:" + x + "," + y + "\n");
            }
        }





    }

}
