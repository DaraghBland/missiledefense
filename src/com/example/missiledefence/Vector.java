package com.example.missiledefence;

public class Vector {

    private float xVel = 1;
    private float yVel = 1;

    public Vector(float xVal, float yVal){
        this.xVel = xVal;
        this.yVel = yVal;
    }

    public Vector() {
    }

    public float getxVel(){
        return xVel;
    }

    public float getyVel(){
        return yVel;
    }

    public void setxVel(float xVel){
        this.xVel = xVel;
    }

    public void setyVel(float yVel){
        this.yVel = yVel;
    }

    public void toggleX(){
        this.xVel *= -1;
    }

    public void toggleY(){
        this.yVel *= -1;
    }
    
    public static double getLength(Vector vector){
    	return Math.sqrt(Math.pow(vector.xVel, 2) + Math.pow(vector.yVel, 2));
    }
    
    public static Vector normalise(Vector vector){
    	float length = (float)Vector.getLength(vector);
    	return new Vector(vector.xVel/length, vector.yVel/length);
    }
    
    public float dotProduct(Vector other){
    	return (this.xVel*other.xVel)+(this.yVel*other.yVel);
    }
    
    public Vector scalarMultiply(float scalar){
    	return new Vector(xVel * scalar, yVel * scalar);
    }

}
