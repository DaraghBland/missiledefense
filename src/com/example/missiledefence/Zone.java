package com.example.missiledefence;

public class Zone {
	
	public static final int VERTICAL = 0;
	public static final int HORIZONTAL = 1;
	
	public static final int INSIDE = 0;
	public static final int OUT_LEFT = 1;
	public static final int OUT_RIGHT = 2;
	public static final int OUT_TOP = 3;
	public static final int OUT_BOTTOM = 4;
	public static final int OUT_OFF_PATH = 5;
	
	public static final int DEF_RANGE = 1500;
	public static final int DEF_ORIENTATION = HORIZONTAL;
	
	private int centerX, centerY;
	private int top, bottom;
	private int left, right;
	private int range = DEF_RANGE;
	private int orientation = DEF_ORIENTATION;
	
	public Zone(){
		init(DEF_ORIENTATION, 200, 200, DEF_RANGE);
	}
	
	public Zone(int orientation){
		init(orientation, 200, 200, DEF_RANGE);
	}
	
	public Zone(int orientation, int centerX, int centerY){
		init(orientation, centerX, centerY, DEF_RANGE);
	}
	
	private void init(int orientation, int centerX, int centerY, int range){
		this.centerX = centerX;
		this.centerY = centerY;
		this.range = range;
		this.orientation = orientation;
		setBounds();
	}
	
	private void setBounds(){
		int halfRange = range/2;
		
		switch(orientation){
		case VERTICAL:
			setLeft(centerX - DEF_RANGE);
			setRight(centerX + DEF_RANGE);
			setTop(centerY - halfRange);
			setBottom(centerY + halfRange);
			break;
		case HORIZONTAL:
			setLeft(centerX - halfRange);
			setRight(centerX + halfRange);
			setTop(centerY - DEF_RANGE);
			setBottom(centerY + DEF_RANGE);
			break;
		}
	}
	
	public int isOutsideRange(int posX, int posY){
		int answer = INSIDE;
		if(posX < left)
			answer = OUT_LEFT;
		else if(posX > right)
			answer = OUT_RIGHT;
		
		if(posY < top)
			answer = OUT_TOP;
		else if(posY > bottom)
			answer = OUT_BOTTOM;
		
		if(getDistance(posX, posY) > range)
			answer = OUT_OFF_PATH;
		
		return answer;
	}
	
	private int getDistance(int posX, int posY){
		int answer = 0;
		
		answer = (int) (Math.pow(centerX - posX, 2) + Math.pow(centerY - posY, 2));
		answer = (int) Math.sqrt(answer);
		
		return answer;
	}
	
	public int getCenterX(){
		return centerX;
	}
	
	public int getCenterY(){
		return centerY;
	}
	
	public int getOrientation(){
		return orientation;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

	public int getBottom() {
		return bottom;
	}

	public void setBottom(int bottom) {
		this.bottom = bottom;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}

}
